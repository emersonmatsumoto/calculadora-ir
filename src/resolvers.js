const scalar = require('./scalar')
const operacao = require('./operacao')
const corretagem = require('./corretagem')

module.exports = {
  ...scalar,
  Query: {
    hello: (parent, args, context) => `Hello ${context.user.email}!`,
    ...operacao.Query
  },
  Mutation: {
    ...operacao.Mutation,
    ...corretagem.Mutation
  }
}

// mutation {
//   setOperacao(
//     input: {
//       data: "2019-01-01"
//       codigo: "ITUB4"
//       cv: "C"
//       quantidade: 100
//       preco: 30.2
//       corretagem: 16.1
//       emolumentos: 0.2
//       imposto: 0.1
//       liquidacao: 0.2
//     }
//   ) {
//     data
//     codigo
//     cv
//     quantidade
//     corretagem
//     emolumentos
//     imposto
//     liquidacao
//   }
// }
