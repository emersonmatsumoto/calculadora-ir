const { createQldbSession } = require('../qldb/session')
const executeLambda = require('../qldb/execute-lambda')
const { insert } = require('../qldb/query-lambda')

const putCorretagem = require('./put')({
  createQldbSession,
  executeLambda,
  queryInsert: insert
})

module.exports = {
  Mutation: {
    putCorretagem
  }
}
