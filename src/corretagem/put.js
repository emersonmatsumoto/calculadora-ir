module.exports = ({
  createQldbSession,
  executeLambda,
  queryInsert
}) => (parent, { input }, { user }) => createQldbSession('carteira')
  .then(executeLambda(queryInsert('corretagem', input)))
