const path = require('path')
const { importSchema } = require('graphql-import')
const { ApolloServer } = require('apollo-server-lambda')
const resolvers = require('./resolvers')

const typeDefs = importSchema(path.join(__dirname, './schema.graphql'))

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ event }) => ({ user: event.requestContext.authorizer.claims })
})

exports.handler = server.createHandler({
  cors: {
    origin: '*',
    credentials: true
  }
})
