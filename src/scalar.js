const { GraphQLScalarType } = require('graphql')

module.exports = {
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',
    parseValue: (value) => new Date(value),
    serialize: (value) => value.toISOString()
  })
}
