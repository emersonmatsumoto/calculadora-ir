const {
  Decimal,
  IonTypes,
  Timestamp
} = require('ion-js')

/**
 * Converts a given value to Ion using the provided writer.
 * @param value The value to covert to Ion.
 * @param ionWriter The Writer to pass the value into.
 * @throws Error: If the given value cannot be converted to Ion.
 */
const writeValueAsIon = (value, ionWriter) => {
  switch (typeof value) {
    case 'string':
      ionWriter.writeString(value)
      break
    case 'boolean':
      ionWriter.writeBoolean(value)
      break
    case 'number':
      ionWriter.writeInt(value)
      break
    case 'object':
      if (Array.isArray(value)) {
        // Object is an array.
        ionWriter.stepIn(IonTypes.LIST)

        for (const element of value) {
          writeValueAsIon(element, ionWriter)
        }

        ionWriter.stepOut()
      } else if (value instanceof Date) {
        // Object is a Date.
        ionWriter.writeTimestamp(Timestamp.parse(value.toISOString()))
      } else if (value instanceof Decimal) {
        // Object is a Decimal.
        ionWriter.writeDecimal(value)
      } else if (value === null) {
        ionWriter.writeNull(IonTypes.NULL)
      } else {
        // Object is a struct.
        ionWriter.stepIn(IonTypes.STRUCT)

        for (const key of Object.keys(value)) {
          ionWriter.writeFieldName(key)
          writeValueAsIon(value[key], ionWriter)
        }
        ionWriter.stepOut()
      }
      break
    default:
      throw new Error(`Cannot convert to Ion for type: ${typeof value}.`)
  }

  return ionWriter
}

module.exports = {
  writeValueAsIon
}
