const { PooledQldbDriver } = require('amazon-qldb-driver-nodejs')

/**
 * Close a QLDB session object.
 * @param session The session to close.
 */
const closeQldbSession = session => {
  if (session) {
    session.close()
  }
}

/**
 * Retrieve a QLDB session object.
 * @returns Promise which fufills with a {@linkcode QldbSession} object.
 */
const createQldbSession = async (ledger) => {
  const qldbDriver = new PooledQldbDriver(ledger, { region: process.env.AWS_REGION })
  const qldbSession = await qldbDriver.getSession()
  return qldbSession
}

module.exports = {
  createQldbSession,
  closeQldbSession
}
