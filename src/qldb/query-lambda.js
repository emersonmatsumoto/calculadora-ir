const { createQldbWriter } = require('amazon-qldb-driver-nodejs')
const { writeValueAsIon } = require('./ion')

const insert = (table, documents) => txn =>
  txn.executeInline(`INSERT INTO ${table} ?`, [
    writeValueAsIon(documents, createQldbWriter())
  ])

module.exports = {
  insert
}
