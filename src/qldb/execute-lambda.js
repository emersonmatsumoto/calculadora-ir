const { closeQldbSession } = require('./session')

module.exports = queryLambda => session =>
  session
    .executeLambda(
      queryLambda,
      () => console.log('Retrying due to OCC conflict...')
    )
    .finally(() => closeQldbSession(session))
