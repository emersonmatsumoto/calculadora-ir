const setKey = (email, codigo, data) =>
  Promise.resolve(
    {
      email,
      tipo: `operacao/${codigo}/${data}`
    })

const getOperacao = dynamodb =>
  Key =>
    dynamodb.get({
      TableName: process.env.carteiraTable,
      Key
    }).promise()
      .then(result => result.Item)

const deleteOperacao = dynamodb =>
  item =>
    dynamodb.delete({
      TableName: process.env.carteiraTable,
      Key: {
        email: item.email,
        tipo: item.tipo
      }
    }).promise()
      .then(() => item)

module.exports = dynamodb =>
  (parent, { input }, { user }) =>
    setKey(user.email, input.codigo, input.data)
      .then(getOperacao(dynamodb))
      .then(deleteOperacao(dynamodb))
