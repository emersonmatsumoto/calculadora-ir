module.exports = dynamodb =>
  (parent, args, { user }) =>
    dynamodb.query({
      TableName: process.env.carteiraTable,
      KeyConditionExpression: 'email = :email',
      ExpressionAttributeValues: {
        ':email': user.email
      }
    }).promise()
      .then(result => result.Items)
