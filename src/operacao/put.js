module.exports = dynamodb =>
  (parent, { input }, { user }) =>
    dynamodb.put({
      TableName: process.env.carteiraTable,
      Item: {
        email: user.email,
        tipo: `operacao/${input.codigo}/${input.data}`,
        ...input
      }
    }).promise()
      .then(() => input)
