const AWS = require('aws-sdk')

const dynamodb = new AWS.DynamoDB.DocumentClient()
const putOperacao = require('./put')(dynamodb)
const listOperacao = require('./list')(dynamodb)
const deleteOperacao = require('./delete')(dynamodb)

module.exports = {
  Query: {
    listOperacao
  },
  Mutation: {
    putOperacao,
    deleteOperacao
  }
}
